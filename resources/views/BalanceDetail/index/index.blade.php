@extends('Master.master')

@section('css-script')

@endsection

@section('content')
  <div class="col-md-10 col-md-offset-1">
    <div class="container-fluid">
      <div class="row">
        <div class="form-group">
            <label for="nama_klien" class="col-md-3 col-form-label">No. SOA/FBO</label>
            <div class="col-md-5">
              <select class="form-control js-example-basic-single" id="nama_klien" name="input[nama_klien]">
                <option value="">All</option>
                <?php foreach ($fbo as $key => $value) { ?>
                <option value="<?= $value->fbo_id ?>"><?= 'TSEL/'.$value->fbo_id.'/'.$value->fbo_number ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
      </div>
      <div class="row">
        
      </div>
    </div>
  </div>
@endsection

@section('js-script')
<script type="text/javascript">
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
  })
</script> 
@endsection