<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/fontawesome-all.min.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Nunito:400,700,800" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/slick.css') }}">
	<link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
	<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.css') }}">
	<!--link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}"-->
	<link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
	<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font_awesome_all.css') }}">
	<link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}">
  @yield('css-script')
	<title>MyAds - Telkomsel</title>
</head>
<!doctype html>
<html lang="en">
<body>
	<div class="onboarding-overlay"></div>
	<div class="onboard" data-onboard="center" data-title="Selamat Datang" data-description="Terima Kasih sudah bergabung dengan MyAds, selamat datang di Dashboard Anda"></div>

	<nav class="navbar navbar-expand-lg navbar-light dashnavbar fixed-top">
		<a class="navbar-brand" href="index.php"><img class="img-logo" src="{{ asset('images/myads-logo-white.png') }}"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link onboard" data-onboard="bottom" data-parent="navbar" data-title="Dashboard" data-description="Dashboard untuk melihat rangkuman akun" href="#">Home</a>
				</li>
				<li class="nav-item">
					<a class="nav-link onboard" data-onboard="bottom" data-parent="navbar" data-title="Template" data-description="Format iklan yang telah disimpan dan dapat digunakan kembali" href="#">Template</a>
				</li>
          <!--
          <li class="nav-item">
            <a class="nav-link onboard" data-onboard="bottom" data-parent="navbar" data-title="Penerima" data-description="Daftarkan nama Penerima yang akan digunakan untuk mengirim iklan" href="#" >Penerima</a>
          </li>
        -->
        <li class="nav-item">
        	<a class="nav-link onboard" data-onboard="bottom" data-parent="navbar" data-title="Pengirim" data-description="Daftarkan nama Pengirim yang akan digunakan untuk mengirim iklan" href="#" >Pengirim</a>
        </li>
        <li class="nav-item">
        	<a class="nav-link onboard" data-onboard="bottom" data-parent="navbar" data-title="Iklan" data-description="Untuk membuat dan melihat iklan yang telah dibuat" href="#" >Iklan</a>
        </li>
        <li class="nav-item dropdown onboard" data-onboard="bottom" data-parent="navbar" data-title="Report" data-description="Melihat secara lengkap laporan iklan Anda">
        	<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
        		Report
        	</a>
        	<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
        		<li class="dropdown-submenu">
        			<a class="dropdown-item" href="#">Campaign</a>
        			<ul class="dropdown-menu pull-left">
        				<li class="dropdown-item"><a href="">Insight</a></li>
        			</ul>
        		</li>
        		<li class="dropdown-item"><a href="#">Balance Top Up</a></li>
        		<li class="dropdown-item"><a href="#">API Activity</a></li>
        	</ul>
        </li>
          <!--
          <li class="nav-item">
            <a class="nav-link onboard" data-onboard="bottom" data-parent="navbar" data-title="Iklan" data-description="Melihat secara lengkap laporan iklan Anda" href="#" >Report</a>
          </li>
        -->
        <li class="nav-item">
        	<a class="nav-link onboard" data-onboard="bottom" data-parent="navbar" data-title="Management" data-description="API Management" href="#" >API Management</a>
        </li>
        <li class="nav-item onboard" data-onboard="bottom" data-parent="navbar" data-title="Buat Iklan" data-description="Mulai buat iklan Anda,. Hanya dapat dipakai setelah akun Anda aktif">
        	<a class="nav-link link-btn btn-white" href="#" data-toggle="modal" data-target="#modalCreateAds" >Buat Iklan</a>
        </li>
      </ul>
      <ul class="navbar-nav">

      	<li class="nav-item dropdown onboard" data-onboard="left" data-parent="navbar" data-title="title" data-description="ini tentang Iklan">
      		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
      			Bantuan
      		</a>
      		<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
      			<li class="dropdown-submenu">
      				<a class="dropdown-item" href="#">Product List</a>
      				<ul class="dropdown-menu pull-left">
      					<li class="dropdown-item"><a href="">Location Based Advertising</a></li>
      					<li class="dropdown-item"><a href="">Targeted</a></li>
      					<li class="dropdown-item"><a href="">Broadcast</a></li>
      					<li class="dropdown-item"><a href="">Contextual</a></li>
      					<li class="dropdown-item"><a href="">Compare Pricing</a></li>
      				</ul>
      			</li>
      			<li class="dropdown-item"><a href="#">Tutorial</a></li>
      			<li class="dropdown-item"><a href="#">FAQ</a></li>
      			<li class="dropdown-item"><a href="#">Contact Us</a></li>
      		</ul>
      	</li>

      	<li class="nav-item profile-img" style="background-image: url(images/ico-profile.png)"></li>

      	<li class="nav-item dropdown onboard" data-onboard="left" data-parent="navbar" data-title="title" data-description="ini tentang Iklan">
      		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
      			Roemah Kopi
      		</a>
      		<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
      			<li class="dropdown-item"><a href="#">Edit Profil</a></li>
      			<li class="dropdown-item"><a href="#">User Files</a></li>
      			<li class="dropdown-item"><a href="#">MSISDN List Group</a></li>
      			<li class="dropdown-item"><a href="#">Campaign Profile Group</a></li>
      			<li class="dropdown-item"><a href="#">Location List Group</a></li>
      			<li class="dropdown-item"><a href="#">Notification</a></li>
      			<li class="dropdown-item"><a href="#">Work Item</a></li>
      			<li class="dropdown-submenu">
      				<a class="dropdown-item" href="#">Saldo</a>
      				<ul class="dropdown-menu pull-left">
      					<li class="dropdown-item"><a href="">History Top Up</a></li>
      					<li class="dropdown-item"><a href="">History Balance</a></li>
      					<li class="dropdown-item"><a href="">Pembayaran</a></li>
      				</ul>
      			</li>
      			<div class="dropdown-divider"></div>
      			<li class="dropdown-item" href="#">Log Out</li>
      		</ul>
      	</li>
      </ul>
    </div>
  </nav>  

  <!-- Content -->
  @section('content')
	Hello World
  @show
  <!-- End Content -->

  <script type="text/javascript" src="{{ asset('js/jquery-1.11.2.min.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('js/popper.min.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('js/slick.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('js/moment.with.local.js') }}" ></script>
  <!--script type="text/javascript" src="{{ asset('js/custom.js') }}" ></script-->
  <script type="text/javascript" src="{{ asset('js/jspdf.min.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('js/jspdf.plugin.autotable.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('js/dataTables.bootstrap.js') }}" ></script>
  <!-- <script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}" ></script> -->
  <script type="text/javascript" src="{{ asset('js/select2.min.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}" ></script>
  <script type="text/javascript" src="{{ asset('js/font_awesome_all.js') }}" ></script>
  @yield('js-script')
</body>  
