@extends('Master.master')

@section('css-script')

@endsection

@section('content')
  <div class="col-md-10 col-md-offset-1">
    <div class="container-fluid">
      <form id="submit_form" action="/balance-top-up" method="POST">
        @csrf
        <div id="input_menu" class="container-fluid">
          <div class="form-group row">
            <label for="nama_klien" class="col-md-3 col-form-label">Pilih Nama Klien</label>
            <div class="col-md-5">
              <select class="form-control js-example-basic-single" id="nama_klien" name="input[nama_klien]">
                <option value="">All</option>
                <?php
                  foreach ($user_child as $key => $value) {
                    echo ("<option value=".$value->user_id.">".$value->user_id."</option>");
                  }
                ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="no_fbo" class="col-md-3 col-form-label">No FBO</label>
            <div class="col-md-5">
              <select class="form-control js-example-basic-single" id="no_fbo" name="input[no_fbo]">
                <option value="">All</option>
                <?php
                  foreach ($fbo as $key => $value) {
                    echo ("<option data-fbo_id=".$value->fbo_id." value=".$value->quotation_id.">".$value->fbo_number."</option>");
                    // echo ("<option value=".$value->fbo_id.">".$value->fbo_number."</option>");
                  }
                ?>
              </select>
            </div>
          </div>
        </div>
        <div id="ringkasan_pembayaran">
          <div class="row">
            <div class="container-fluid">
              <h4>SISA BALANCE PRODUK</h4>
            </div>
          </div>
          <div class="container-fluid">
            <div class="col-md-12 table-responsive">
              <table id="history_top_up_table" class="table table-striped table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Produk</th>
                    <th>Kuantitas</th>
                    <th>Harga</th>
                    <th>Jumlah</th>
                    <th>Sisa</th>
                    <th>Periode Mulai</th>
                    <th>Periode Akhir</th>
                    <th>FBO</th>
                    <th>Client Name</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="container-fluid">
              <h4>PEMBELIAN PRODUK</h4>
            </div>
          </div>
          <div>
            <div class="col-md-6">
              <div class="form-group row">
                <label for="produk" class="col-md-4 col-form-label">Produk</label>
                <div class="col-md-8">
                  <select class="form-control js-example-basic-single" id="produk" name="input[produk]"></select>
                </div>
              </div>
              <div class="form-group row">
                <label id="invetory_label" for="jumlah" class="col-md-4 col-form-label">Kuantitas</label>
                <div class="col-md-8">
                  <input min="0" type="number" class="form-control only-number" id="jumlah" name="input[jumlah]" value="" autocomplete="off"><br>
                  <label><span id="sisa_kuantitas_label">Sisa Kuantitas : </span><span id='sisa_kuantitas' style='color: red;'></span></label>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-md-4 col-form-label">Periode</label>
                <div class="col-md-8">
                  <div class="input-group pull-right">
                    <input type="text" id="create-from-date" class="form-control datepicker" name="input[from]" data-date-format="dd/mm/yyyy" autocomplete="off">
                    <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </span>
                    <input type="text" id="create-to-date" class="form-control datepicker" name="input[to]" data-date-format="dd/mm/yyyy" autocomplete="off">
                  </div>
                </div>
              </div>
              <div class="form-group row">
              <label class="col-md-4 col-form-label">ROB Number</label>
              <div class="col-md-8">
                  <input type="text" class="form-control alphanumeric" id="rob_number" name="input[rob_number]" value="" autocomplete="off">
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-10"></div>
              <div class="col-md-2">
                <button type="button" id="add_product" class="btn btn-primary btn-block" style="margin-bottom: 20px"><i class="fa fa-plus"></i> Tambah</button>
              </div>
            </div>
          </div>
          <div class="container-fluid">
            <div class="col-md-12 table-responsive">
              <table id="history_top_up_add_table" class="table table-striped table-bordered" style="width:100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>ROB No</th>
                    <th>Produk</th>
                    <th>Kuantitas</th>
                    <th>Harga</th>
                    <th>Periode Mulai</th>
                    <th>Periode Akhir</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
        <div class="">
          <div class="col-md-8">
            
          </div>
          <div class="col-md-4">
            <button class="btn btn-primary pull-right" id="save_button">Save/Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('js-script')
<script type="text/javascript">
  var _global = Array;
  $(document).ready(function() {
    _global.history_top_up_table = $('#history_top_up_table').DataTable({
        "serverSide": true,
        "ajax": {
          "url" : "/balance-top-up/req",
          'type': 'GET',
          "data": function ( d ) {
            d.client = $('#nama_klien option:selected').val();
            d.quot_no = $('#no_fbo option:selected').val();
          }
        },
        "rowId" : 'product_order_id',
        "columns": [
          { "data": "product_order_id" },
          { "data": "product_name" },
          { "data": "balance_quantity" },
          { "data": "product_price" },
          { 
            "data": null,
            "render" : function(data, type, row, meta) {
              var quantity = data.quantity * data.product_price;
              return quantity;  
            }
          },
          { "data": "balance_currency" },
          { "data": "start_period" },
          { "data": "end_period" },
          { 
            "data": null,
            "render" : function(data, type, row, meta) {
              return data.fbo_number;
            }
          },
          { "data": "quotation_client_id" }
        ],
    });

    $('#jumlah').on('keyup', function() {
        if ($(this).val() !== '') {
            var _res = _blc_qty - parseInt($(this).val());
            $('#sisa_kuantitas').text(_res);
        }
        else {
            $('#sisa_kuantitas').text(_blc_qty);
        }
    })

    _global.history_top_up_add_table = $('#history_top_up_add_table').DataTable( {
      "rowId" : 'id',
      "columns": [
      { 
        "data": null,
        "render" : function(data, type, row, meta) {
          var input_id = "<input class='hidden' name=table[" + data.id + "][id] value=" + data.id + ">";
          var input_client = "<input class='hidden' name=table[" + data.id + "][client_id] value=" + data.client_id + ">";
          var input_fbo = "<input class='hidden' name=table[" + data.id + "][fbo_id] value=" + data.fbo_id + ">";

          var total = 0;
          if (data.inventory_type === 'SPECIFIC') {
            total = data.total;
            var input_blc_qty = "<input class='hidden' name=table[" + data.id + "][blc_qty] value=" + data.blc_qty + ">";
            var input_blc_curr = "<input class='hidden' name=table[" + data.id + "][blc_curr] value=" + data.blc_curr + ">";
          } else if (data.inventory_type === 'OPEN') {
            total = data.total;
            var input_blc_qty = "<input class='hidden' name=table[" + data.id + "][blc_qty] value=" + data.blc_qty + ">";
            var input_blc_curr = "<input class='hidden' name=table[" + data.id + "][blc_curr] value=" + data.blc_curr + ">";
          };
          var input_total = "<input class='hidden' name=table[" + data.id + "][total] value=" + total + ">";
          
          return input_id + input_client + input_fbo + (meta.row + 1) + input_total + input_blc_qty + input_blc_curr;
        }
      },
      { 
        "data": null,
        "render" : function(data, type, row, meta) {
          return "<input class='hidden' name=table[" + data.id + "][rob_no] value=" + data.rob_no + ">" + data.rob_no;
        }
      },
      { 
        "data": null,
        "render" : function(data, type, row, meta) {
          return "<input class='hidden' name=table[" + data.id + "][produk] value=" + data.produk_id + ">" + data.produk;
        }
      },
      { 
        "data": null,
        "render" : function(data, type, row, meta) {
          var jumlah = data.jumlah;
          var input_jumlah = "<input class='hidden' name=table[" + data.id + "][jumlah] value=" + jumlah + ">";
          return input_jumlah + jumlah;
        }
      },
      { 
        "data": null,
        "render" : function(data, type, row, meta) {
          var harga = data.harga
          var input_harga = "<input class='hidden' name=table[" + data.id + "][harga] value=" + harga + ">";
          return input_harga + harga;
        }
      },
      // { 
      //   "data": null,
      //   "render" : function(data, type, row, meta) {
      //     var total = 0;
      //     if (data.inventory_type === 'SPECIFIC') {
      //       total = data.total;
      //       var input_blc_qty = "<input class='hidden' name=table[" + data.id + "][blc_qty] value=" + data.blc_qty + ">";
      //       var input_blc_curr = "<input class='hidden' name=table[" + data.id + "][blc_curr] value=" + data.blc_curr + ">";
      //     } else if (data.inventory_type === 'OPEN') {
      //       total = data.jumlah;
      //       var input_blc_qty = "<input class='hidden' name=table[" + data.id + "][blc_qty] value=" + data.blc_qty + ">";
      //       var input_blc_curr = "<input class='hidden' name=table[" + data.id + "][blc_curr] value=" + data.blc_curr + ">";
      //     };
      //     var input_total = "<input class='hidden' name=table[" + data.id + "][total] value=" + total + ">";
      //     return input_total + data.total + input_blc_qty + input_blc_curr;
      //   }
      // },
      { 
        "data": null,
        "render" : function(data, type, row, meta) {
          return "<input class='hidden' name=table[" + data.id + "][start_period] value=" + data.periode_mulai + ">" + data.periode_mulai;
        }
      },
      { 
        "data": null,
        "render" : function(data, type, row, meta) {
          return "<input class='hidden' name=table[" + data.id + "][end_period] value=" + data.periode_akhir + ">" + data.periode_akhir;
        }
      },
      { 
        "data": null,
        "render" : function(data, type, row, meta) {
          return '<a><i class="fas fa-trash-alt action_delete"></i></a>';
        }
      },
      ],
    });

    // $('.date-picker').datepicker({
    //   orientation: "left",
    //   autoclose: true,
    //   dateFormat: 'dd-mm-yy'
    // });
    var _blc_qty = 0;

    $('#produk').change(function() {
      var id = $(this).val();
      var row = _global.history_top_up_table.row('#'+id).data();

      $('#jumlah').val('');
      $('#create-from-date').val('');
      $('#create-to-date').val('');
      $('#rob_number').val('');
      
      if (row) {
        if (row.inventory_type === 'SPECIFIC') {
          _blc_qty = row.balance_quantity;
          $('#sisa_kuantitas').text(_blc_qty);
          $('#sisa_kuantitas_label').text('Sisa Kuantitas : ');
          //set max value input on Kuantitas input
          $('#invetory_label').text('Kuantitas');
          $('#jumlah').on('input', function () {
            var value = $(this).val();
            if ((value !== '') && (value.indexOf('.') === -1)) {
              $(this).val(Math.max(Math.min(value, row.balance_quantity), 0));
            }
          });
        } else if (row.inventory_type == 'OPEN') {
          _blc_qty = row.balance_currency;
          $('#sisa_kuantitas').text(_blc_qty);
          $('#sisa_kuantitas_label').text('Sisa Harga : ');
          //$('#jumlah').attr('name', 'input[price]');
          $('#invetory_label').text('Harga (Rp)');
          $('#jumlah').on('input', function () {
            var value = $(this).val();
            if ((value !== '') && (value.indexOf('.') === -1)) {
              $(this).val(Math.max(Math.min(value, row.balance_currency), 0));
            }
          });
        }
      }
    });

    $( "#add_product" ).click(function() {
      if (($('#create-from-date').val() != "") && ($('#create-to-date').val() != "") && ($('#produk option:selected').val() != null) && (parseInt($('#jumlah').val()) > 0) ) {
        var id_product_order = $('#produk option:selected').val();
        var check_row = _global.history_top_up_add_table.row('#'+id_product_order).data();

        if (check_row == null) {
          var row_data = _global.history_top_up_table.row('#'+id_product_order).data();
          var id = id_product_order;
          var rob_no = $('#rob_number').val();
          var produk = row_data.product_name;
          var produk_id = row_data.product_id;
          var jumlah = parseInt( $('#jumlah').val() );
          var harga = parseInt(row_data.product_price);
          var total;
          if (row_data.inventory_type === 'SPECIFIC') {
            total = jumlah * harga;
            var blc_qty = row_data.balance_quantity - jumlah;
            var blc_curr = row_data.balance_currency - (jumlah * harga);
          } else if (row_data.inventory_type === 'OPEN') {
            total = jumlah * harga;
            var blc_qty = row_data.balance_quantity - (jumlah / row_data.product_price);
            var blc_curr = row_data.balance_currency - jumlah;
          }
          var periode_mulai = $('#create-from-date').val();
          var periode_akhir = $('#create-to-date').val();
          // var fbo_id = $('#no_fbo').val();
          var fbo_id = $('#no_fbo option:selected').data('fbo_id');
          var client_id = $('#nama_klien').val();
          var inventory_type = row_data.inventory_type;

          _global.history_top_up_add_table.row.add({
            id,
            rob_no,
            produk,
            produk_id,
            jumlah,
            harga,
            total,
            periode_mulai,
            periode_akhir,
            fbo_id,
            client_id,
            inventory_type,
            blc_qty,
            blc_curr
          }).draw(false);
        }
      }
      return;
    });

    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
    });

    $('.js-example-basic-single').select2();

    $(".only-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
             (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
             (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
               return;
             }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
        }
      });

    String.prototype.replaceAll = function(search, replacement) {
      var target = this;
      return target.replace(new RegExp(search, 'g'), replacement);
    };

    $('#nama_klien, #no_fbo').on('change', function () {
      _global.history_top_up_table.ajax.reload();

      $('#produk').find('option').remove().end();

      if ( $('#nama_klien option:selected').val() !== '' && $('#no_fbo option:selected').val() !== ''  ) {
        $.getJSON("/balance-top-up/req?client=" + $('#nama_klien option:selected').val() + "&" + "quot_no=" + $('#no_fbo option:selected').val(), function(result){
          var old_product = null;
          $.each(result.data, function(i, field) {
            if ( field.product_order_id != old_product ) {
              var newOption = new Option('('+field.product_order_id+') '+field.product_name, field.product_order_id, true, false);
              $('#produk').append(newOption);
              old_product = field.product_order_id;
            }
          });
          $('#produk').val(null).trigger('change');
        });
      }
    });

    $('#create-from-date').on('change', function() {
      var temp = this.value;
      var date_to = $('#create-to-date').val();
      if (date_to != null) {
        var to = string_to_date(date_to);
        var now = string_to_date(temp);
        if (now > to) {
          $('#create-from-date').val('');
        }
      }
    });

    $('#create-to-date').on('change', function() {
      var temp = this.value;
      var date_to = $('#create-from-date').val();
      if (date_to != null) {
        var to = string_to_date(date_to);
        var now = string_to_date(temp);
        if (now < to) {
          $('#create-to-date').val('');
        }
      }
    });

    $('#history_top_up_add_table tbody').on( 'click', '.action_delete', function () {
        var id = $(this).parents('tr');
        _global.history_top_up_add_table.row(id).remove().draw();
    });

    $('.alphanumeric').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });

    $('#submit_form').validate({
        submitHandler: function(form) {
            if (_global.history_top_up_add_table.rows().data().length > 0) {
                form.submit();
            }
        }
    });
    
  }); //jquery on ready end here

    function string_to_date(input) {
        var res = input.split("-");
        var result = new Date(res[2], res[1], res[0]);
        return result;
    }
</script> 
@endsection