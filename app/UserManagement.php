<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserManagement extends Model
{
   protected $table = 'user_management';

   protected $hidden = [
        'user_management_password', 'remember_token',
    ];
}
