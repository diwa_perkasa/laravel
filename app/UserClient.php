<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClient extends Model
{
   protected $table = 'user_client';

   protected $hidden = [
        'password', 'remember_token',
    ];
}
