<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
//model
use App\UserClient;
use App\UserManagement;
use App\ProductOrder;
use App\PricebookSetting;
use App\Quotation;
use App\FBO;

class BalanceTopUp extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */

    public function __construct()
    {
      //$this->middleware('auth');
    }

    public function index() {
        $user_login = 'doyokms3@gmail.com';

        session(['email' => $user_login]);

        $user_man = UserClient::where('email', $user_login)->get();
        $user_child = UserClient::where('parent_user', $user_man[0]->user_id)->get();
        $user_child_list =[];

        foreach ($user_child as $user_child_key => $user_child_value) {
            $user_child_list[] = $user_child_value->user_id;
        }

        session(['user_child' => $user_child_list]);

        $product_order = DB::table('product_order')
            ->join('quotation', 'product_order.product_order_quotation_id', '=', 'quotation.quotation_id')
            ->whereIn('quotation_client_id', session('user_child'))
            ->select('product_order.*', 'quotation.quotation_client_id', 'quotation.quotation_number')
            ->get();

        $fbo = DB::table('fbo')
            ->join('quotation', 'fbo.quotation_id', '=', 'quotation.quotation_id')
            ->select('fbo.*', 'quotation.quotation_id', 'quotation.quotation_number', 'quotation.payment_type')
            ->get();

        return view('BalanceTopUp.index.index', [
            'product' => $product_order,
            'fbo' => $fbo,
            'user_child' => $user_child,
        ]);
    }

    public function store(Request $request) {
      $input = $request['input'];
      $table = $request['table'];

      if (isset($table)) {
        foreach ($table as $table_key => $table_value) {

            $rob_id = DB::table('rob')->insertGetId([
                'rob_number' => $table_value['rob_no'], 
                'rob_client_id' => $table_value['client_id'],
                'fbo_id' => $table_value['fbo_id'],
                'created_by' => 'system'
            ]);

            $rob_product_id = DB::table('rob_product')->insertGetId([
                'rob_product_rob_id' => $rob_id, 
                'product_order_id' => $table_value['id'],
                'product_price' => $table_value['harga'],
                'quantity' => $table_value['jumlah'],
                'gross' => $table_value['total'],
                'start_period' => date( "Y-m-d H:i:s", strtotime($table_value['start_period'])),
                'end_period' => date( "Y-m-d H:i:s", strtotime($table_value['end_period'].' 23:59:59')),
                'created_by' => 'system'
            ]);

            $prod_order = DB::table('product_order')
                ->where('product_order_id', $table_value['id'])
                ->update([
                    'balance_quantity' => $table_value['blc_qty'],
                    'balance_currency' => $table_value['blc_curr']
                ]);
        }
    }
        return redirect('/balance-top-up');
    }

    public function create() {
        return response()->json([
            'name' => 'show',
            'state' => 'Noxus'
        ]);
    }

    public function show($id) {
        $product_order = [];
        return 'from show'; 
    }

    public function req(Request $request) {
        $user_login = session('email');

        $limit = $request->input('length') ? $request->input('length') : 100;
        $offset = $request->input('start') ? $request->input('start') : 0;

        $where = [];

        if ($request->input('client')) {
            if ($request->input('client') !== '') {
                $where[] = ['quotation.quotation_client_id', '=', $request->input('client')];
            }
        }

        if ($request->input('quot_no')) {
            if ($request->input('quot_no') !== '') {
                $where[] = ['product_order.product_order_quotation_id', '=', $request->input('quot_no')];
            }
        }

        // Bonus cannot be showed
        $where[] = ['product_order.price_type', '!=', 'BONUS'];

        $product_order = DB::table('product_order')
            ->join('quotation', 'product_order.product_order_quotation_id', '=', 'quotation.quotation_id')
            ->join('fbo', 'fbo.quotation_id', '=', 'quotation.quotation_id')
            ->join('master_product', 'product_order.product_id', '=', 'master_product.master_product_id')
            ->whereIn('quotation_client_id', session('user_child'))
            ->where($where)
            ->select('product_order.*', 'fbo.fbo_id', 'fbo.fbo_number', 'quotation.quotation_number', 'quotation.quotation_client_id', 'master_product.master_product_name as product_name', 'master_product.inventory_type as inventory_type')
            ->offset($offset)
            ->limit($limit)
            ->get();

        $filtered = DB::table('product_order')
            ->join('quotation', 'product_order.product_order_quotation_id', '=', 'quotation.quotation_id')
            ->whereIn('quotation_client_id', session('user_child'))
            ->where($where)->count();

        $total = DB::table('product_order')->count();

        $res = (object)array();

        $res->draw = $request->input('draw') ? $request->input('draw') : 0;
        $res->recordsTotal = $total;
        $res->recordsFiltered = $filtered;
        $res->data = $product_order;

        return response()->json($res);
    }

    public function update(Request $request, $id) {      
        if ($request->isMethod('post')) {
            $input = $request->all();
            return $input;
        }
    }

    public function destroy($id) {      
        if ($request->isMethod('post')) {
            $input = $request->all();
            return $input;
        }
    }

    public function edit($id) {      
        if ($request->isMethod('post')) {
            $input = $request->all();
            return $input;
        }
    }

  }