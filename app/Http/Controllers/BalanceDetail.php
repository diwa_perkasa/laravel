<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BalanceDetail extends Controller
{
    //
    public function __construct()
    {
      //$this->middleware('auth');
    }

    public function index()
    {
        $user_login = 'rendasantana@gmail.com';

        session(['email' => $user_login]);

        $user = DB::table('user_management')
            ->where('user_management_email', $user_login)
            ->get();

        $user_client = DB::table('user_client')
            ->where('parent_group', $user[0]->user_management_group_id)
            ->get();

        $user_client_list = [];

        foreach ($user_client as $key => $value) {
            $user_client_list[] = $value->user_id;
        }

        $fbo = DB::table('fbo')
            ->whereIn('fbo_client_id', $user_client_list)
            ->get();

        return view('BalanceDetail.index.index', [
            'fbo' => $fbo,
        ]);
    }

    public function req(Request $request)
    {
        return response()->json($request);
    }
}
