<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model
{
	protected $table = 'product_order';
	protected $primaryKey = 'product_order_id';

  public function quotation()
  {
      return $this->hasOne('App\Quotation', 'quotation_id');
  }

  public function fbo()
  {
      return $this->hasOne('App\FBO', 'fbo_id');
  }
}
