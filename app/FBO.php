<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FBO extends Model
{
	protected $table = 'fbo';
	protected $primaryKey = 'fbo_id';

	public function product_order()
  {
      return $this->belongsTo('App\ProductOrder', 'product_order_soa_id');
  }

  function getFboIdxAttribute() {
  	return str_pad($this->fbo_id,7,'0',STR_PAD_LEFT);
  }
}
