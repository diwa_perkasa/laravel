<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
  protected $table = 'quotation';
  protected $primaryKey = 'quotation_id';

	public function product_order()
  {
  	return $this->belongsTo('App\ProductOrder', 'product_order_quotation_id');
  }
}
