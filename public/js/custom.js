swidth = $(window).width();
sheight = $(window).height();
var threshold = 0;

if(swidth <= 540){ threshold = 20; } else { threshold = 100; }

function menuscrolled(){
    if($('.mynavbar').length){
        if($(window).scrollTop() >  100 ){
            $('nav.navbar').addClass('scrolled');
            $('nav.navbar').find('.img-logo').attr('src','images/myads-logo-color.png')
            $('nav.navbar').find('.user-login .btn-grey').removeClass('btn-grey').addClass('btn-yellow');
            $('nav.navbar').find('.user-login .btn-transparent').removeClass('btn-transparent').addClass('btn-border-yellow');
        }else{
            $('nav.navbar').removeClass('scrolled');
            $('nav.navbar').find('.img-logo').attr('src','images/myads-logo-white.png')
            $('nav.navbar').find('.user-login .btn-yellow').removeClass('btn-yellow').addClass('btn-grey');
            $('nav.navbar').find('.user-login .btn-border-yellow').removeClass('btn-border-yellow').addClass('btn-transparent');
        }    
    }
}

jQuery(document).ready(function($){
    menuscrolled();


    // $('.custom-control-input').prop('indeterminate', true)
    $('[data-toggle="tooltip"]').tooltip()

    //EFEK ANIMASI ELEMEN UNTUK HOMEPAGE
    $(".animated").each(function(){
        elem = $(this);
        if( ( elem.offset().top - $(window).scrollTop() ) <= $(window).height()-threshold ){
                elem.addClass(elem.attr('anim'));            
        }else{
            elem.css('opacity',0);
        }
    });

    

    //STEPS TUTORIAL DI HOME
    $('.steps li').click(function() {
        $('.steps li').removeClass('active');
        var idx = $('.steps li').index($(this));
        $(this).addClass('active');
        $('.img-monitor').attr('src','images/monitor'+(idx+1)+'.png');
        
    });

    if( !('.sticky-off').length){
        $('fieldset.summary-cost').css('top',$('fieldset.summary-message').height()+80 );
    }

    //DUMMY AUTO CREATE tinyURL
    $('#inserturl').click(function(e) {
        var val = $("#ad-text").val() + " http://tsel/aSdasF";
        $("#ad-text").val( val );
        e.preventDefault();
    });

    //CREATE NEW EDITOR for multidemand
    var ctrmessage = 1; // counter multidemand
    $('#newmessage').click(function(e) {
        var temp = $('fieldset.create-message-area').html();
        temp = temp + "<i class='close-editor fas fa-times-circle font-orange'></i>";
        ctrmessage = ctrmessage+1;
        $('#addmessage').append('<fieldset id="'+ ctrmessage +'">'+temp+'</fieldset>');
        $('fieldset#'+ctrmessage).hide().fadeIn('fast');
    });

    //REVEAL PASSWORD
    $('.reveal-pass i').click(function(e) {
        var elem = $(this).toggleClass("fa-eye fa-eye-slash");
        var change = $(this).hasClass("fa-eye-slash") ? "text" : "password";
        $(this).parent().siblings('input').attr('type',change);    
    });

    //TOGGLE CHECKBOX
    $('.checkbox-toggle').click(function(e) {
        $(this).find('.icon-tick').toggleClass('fa-square fa-check-square');
    });

    $('.advance-profile-checkbox').click(function(e) {
        var target = "fieldset."+$(this).attr('data-target');
        // $(this).parent().parent().find('fieldset.life-segment').slideToggle();
        console.log($(this).parent().siblings(target).slideToggle('fast'));
    });

    $('.save-profile-checkbox').click(function(e) {
        $('.profile-inputname').fadeToggle();
    });

    $('.summary-accordion .btn').click(function(e) {
        console.log('tes');
        $(this).find('i').toggleClass("fa-chevron-down fa-chevron-up");
    });

    //CEK SYARAT DAN KETENTUAN
    $('#agreement').change(function(e) {
        if ( $(this).is(":checked") )
        {
          $('#final-next').prop("disabled", false);
        } else{
          $('#final-next').prop("disabled", true);
        }       
    });

    $('#final-next').click(function(e) {
        e.preventDefault();
    });    

    $('form.user-log').submit(function(event) {
        var formData = {
            'form-source'      : $('input[name=form-source]').val()
        };

        $.ajax({
            type        : 'POST', 
            url         : 'login-register.php', 
            data        : formData,
            // dataType    : 'json',
            // encode      : true
        }).done(function(data) {
            $('#modalNotification .modal-body').html(data);
            $('#modalNotification').modal({
                'backdrop' : 'static'
            });
                // console.log(data); 
        });  
        event.preventDefault();      
    });

    //PREVIEW EDITOR
    //PREVIEW MESSAGE DI LAYAR HP
    $(document).on('click','.btn-preview',function(e) {
        var instyle = "";
        if($(this).hasClass('template-ussd')){
            var text = $(this).closest('fieldset').find('.ad-text').find('textarea').val();
            var addtext = "<div class='closelink'>CLOSE</div>";
            $(this).closest('fieldset').find('.ad-content').addClass('ussd');
            $(this).closest('fieldset').find('.ad-content').hide().fadeIn('fast');
            $(this).closest('fieldset').find('.ad-content').html(text+addtext);
            
        }else if($(this).hasClass('template-sms')){
            var text = $(this).closest('fieldset').find('.ad-text').find('textarea').val();
            $(this).closest('fieldset').find('.ad-content').removeClass('ussd');
            $(this).closest('fieldset').find('.ad-content').hide().fadeIn('fast');
            $(this).closest('fieldset').find('.ad-content').html(text);
            
        }else if($(this).hasClass('template-mms')){
            console.log('mms');
            var subject = $('input.mms-subject').val();
            var text = $(this).closest('fieldset').find('.ad-text').find('textarea').val();
            var image = $(this).closest('fieldset').find('.upload-area').css('background-image');
            console.log(image);
            if(image.length > 4) {
                image = image.replace('url(','').replace(')','').replace(/\"/gi, "");
                image = '<img src="'+image+'">';
            }else{
                image = '<img src="images/img-noimage.jpg">';
            }
            $(this).closest('fieldset').find('.ad-content').removeClass('ussd');
            $(this).closest('fieldset').find('.ad-content').hide().fadeIn('fast');
            $(this).closest('fieldset').find('.ad-content').html(subject+"<hr>"+image+"<br><br>"+text);

        }else{
            var text = $(this).closest('fieldset').find('.ad-text').find('textarea').val();
            $(this).closest('fieldset').find('.ad-content').hide().fadeIn('fast');
            $(this).closest('fieldset').find('.ad-content').html(text);
        }
        // $(this).parent().parent().siblings().find('.ad-content').hide().fadeIn('fast');
        // $(this).parent().parent().siblings().find('.ad-content').html(text);
    });    

    //Aktivasi Delivery report
    $(document).on('click','.activate-dlr-btn',function(e) {
        $(this).find('.icon-tick').toggleClass('fa-square fa-check-square');
    });    

    //SHOW GIVE REWARD DI BAWAH EDITOR
    $(document).on('click','.give-reward-btn',function(e) {
        $(this).find('.icon-tick').toggleClass('fa-square fa-check-square');
        $(this).parent().parent().siblings('.give-reward-wrapper').slideToggle();
    });    

    //SHOW ADS LINK DI BAWAH EDITOR
    $(document).on('click','.ads-link-btn',function(e) {
        $(this).find('.icon-tick').toggleClass('fa-square fa-check-square');
        $(this).parent().parent().siblings('.ads-link-wrapper').slideToggle();
    });    

    $(document).on('click','.checkbox-custom',function(e) {
        $(this).find('.icon-tick').toggleClass('fa-square fa-check-square');
    });    

    $(document).on('click','.pretargeting-ads',function(e) {
        var tes = $(this).parent().parent().siblings('.pretargeting-select').toggle();
    });    

    //DELETE CREATED EDITOR
    $(document).on('click','.close-editor',function(e) {
        $(this).parent().fadeOut('fast',function(){
          $(this).remove();  
          ctrmessage = ctrmessage - 1;
        })
    });    

    // $("input[name=template-options]").change(function() {
    //     console.log($(this));
    //     if( typeof( $(this).attr('data-modal') ) != 'undefined' ) { console.log('tes') }        ;
    //     if( $("#template2").is(":checked") ){
    //         $('#modalTemplate').modal('show');
    //     }
    // });

    $(".select-template input").change(function() {
        var elem = $(this).parent().parent().find('input:radio[name=template-options]');
        var idx = elem.index(elem.filter(':checked'));

        if( idx == 1 ){
            $('#modalTemplate').modal('show');            
            $(".change-template").show();
        }else{
            $(".change-template").hide();
        }
    });

    $('#modalTemplate').on('hidden.bs.modal', function (e) {
      // console.log(e);
    })

    if($('.mynavbar').length){
        $('.slider-portfolio').slick({
            slidesToShow: 6,
            responsive: [
                {
                  breakpoint: 540,
                  settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                  }
                }
              ]            

        });
    };

    if($('.dashboard-promo').length){
        $('.dashboard-promo').slick({
            slidesToShow: 4,
            dots: true,
            slidesToScroll: 4            
        });
    }

    //IF MULTIPLE DEMAND SHOW TOMBOL ADDMESSAGE
    $("input[name=multiple]").change(function(){
        $(".notes").find("div").toggleClass("active");
        $(".group-content").toggle();
        $(".single-content").toggle();
    });

    //SELECTOR RANGE LOCATION
    $("input:radio[name='range-type']").change(function(){
        var selected = $("input:radio[name='range-type']");
        var radioIndex = selected.index(selected.filter(':checked'));
        if(radioIndex==0){
            $(".range-range").hide(); $(".range-radius").show();
        }else{
            $(".range-radius").hide(); $(".range-range").show();            
        }
    });

    //SELECTOR PROFILE
    $("input:radio[name*=selector-profile]").change(function(){
        var selected = $(this).parent().parent().parent();
         $(selected).siblings('.profile-old').toggle();
         $(selected).siblings('.profile-new').toggle();
    });

    if($('.testimonial-slider').length){

        $('.product-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            dots: true,
            arrows: false,
            responsive: [
                {
                  breakpoint: 540,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                  }
                }
              ]            
        });

        $('.testimonial-slider').slick({
            slidesToShow: 1,
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 5000
        });
    }

    //DUMMY SET COUNTER JUMLAH PESAN DI PAGE BERIKUTNYA
    $('#form-step-1').submit(function(e){
        $('input[name=totalmessage]').attr('value',ctrmessage);
    })

    var sliders = $('.slider');
    function createSlider (slide) {
        var slider = noUiSlider.create(sliders[slide], {
            start: [2000000,8000000],
            step: 1000,
            connect: true,
            orientation: "horizontal",
            range: {
                'min': 0,
                'max': 10000000
            },
        });
        
        $(sliders[slide]).data('slider', slider);
        
        slider.on('update', function( values, handle) {
            //IF GERAKIN SLIDER BAR MINIMUM
            if(handle==0){ $(this.target).next().find('.arpu-min').val(parseInt(values[0]));}
            //IF GERAKIN SLIDER BAR MAKSIMUM
            if(handle==1){ $(this.target).next().find('.arpu-max').val(parseInt(values[1]));}

        });
    }

    if($(".slider").length){
        sliders = document.getElementsByClassName('slider');
        for ( var i = 0; i < sliders.length; i++ ) {
            createSlider(i);
        }
    }

    $(document).on('click','.btn-addlocation',function(e) {
        var send = {showdata:'addlocation'};
        $.post( "../../../showdata.php", send)
        .done(function( data ) {
            $( ".location-list" ).prepend( data );

        });            
        $('#modalLocation').modal('hide');
    });

    $(document).on('click','.btn-selectloc',function(e) {
        var send = {showdata:'newlocation'};
        $.post( "../../../showdata.php", send)
        .done(function( data ) {
            $( ".location-list" ).hide().html( data ).fadeIn(); 
            $(".calculation").find('.calculate-value span').html('10.000 - 20.000');
            $(".alert-calculate").fadeIn();
        });            
        $('#modalLocation').modal('hide');
    });

    $(document).on('click','.btn-selectloc-lba',function(e) {
        var send = {showdata:'newlocation-lba'};
        $.post( "../../../showdata.php", send)
        .done(function( data ) {
            $( ".location-list" ).hide().html( data ).fadeIn(); 
            $(".calculation").find('.calculate-value span').html('10.000 - 20.000');
            $(".alert-calculate").fadeIn();
        });            
        $('#modalLocation').modal('hide');
    });

    $(document).on('click','.add-profile',function(e) {
        // var send = {showdata:'newprofile'};
        // $.post( "../../../showdata.php", send)
        // .done(function( data ) {
        //     $( ".archive-profile" ).html( data ).css('margin-top','20px');   
        // });            
        $("fieldset.create-profile").slideToggle();
        e.preventDefault();

    });

    $(document).on('click','.save-profile',function(e) {
        var send = {showdata:'addprofile'};
        $.post( "../../../showdata.php", send)
        .done(function( data ) {
            $( ".archive-profile" ).prepend( data );   
        });            
        $(this).closest('fieldset').slideToggle();
    });

    $(document).on('click','.btn-save-group',function(e) {
        var send = {showdata:'newgroup'};
        $.post( "../../../showdata.php", send)
        .done(function( data ) {
            $( ".archive-group" ).prepend( data ).find('.body').first().hide().fadeIn().css('margin-top','20px');   
            $(".morelocation").empty();
            $(".moreprofile").empty();
        });            
        e.preventDefault();
    });


    var invoker = "";
    $('#modalLocation').on('show.bs.modal', function (e) {
        invoker = $(e.relatedTarget);
        invokerId = invoker.attr('id');      

    });

    if($('.tutorial-counter').length){
        var ctr = $('.tutorial-counter li').length;
        if(ctr >= 5 ){ctr = 5}
        $('.tutorial-counter').slick({
            accessibility: false,
            vertical : true,
            slidesToShow : ctr,
            slidesToScroll : ctr,
            infinite : false,
            dots : true,
            adaptiveHeight: true,
            verticalSwiping : true
            
        });  
    }

    $(".tutorial .tutorial-counter .slick-dots").offset.top 

    // $('.tutorial-counter .slick-dots li').click(function(e) {
    //     $('html,body').animate({
    //         scrollTop: $('#subtitle').offset().top
    //     }, 700);    
    // });

    $("#inputPenerima").change(function(){
        console.log('masuk');
        var file = $('#inputPenerima')[0].files[0];
        if (file){ $(".outputfile").html(file.name) }
    });

    //Load menu setelah pilih SMS / MMS / USSD / Display
    $(document).on('click','.option-item',function(e) {
        var thtml = "";
        var option = $('.create-ads-option .row .option-item').index($(this));
        $.getJSON("menu.json",{}, function( data ){ 
            switch(option){
                case 0: 
                    jQuery.each(data['broadcast'], function(i, val) {
                      thtml += "<div class='option-item option-subitem col'>"
                      thtml += "<a href='"+val.href+"'>"
                      thtml += "<div class='image'><img src=images/"+val.image+"></div>"
                      thtml += "<div class='title'>"+val.title+"</div>"
                      thtml += "<div class='description'>"+val.description+"</div>"
                      thtml += "<div class='price'>"+val.price+"</div>"
                      thtml += "</a></div>"
                    });
                break;
                case 1: 
                    jQuery.each(data['lba'], function(i, val) {
                      thtml += "<div class='option-item option-subitem col'>"
                      thtml += "<a href='"+val.href+"'>"
                      thtml += "<div class='image'><img src=images/"+val.image+"></div>"
                      thtml += "<div class='title'>"+val.title+"</div>"
                      thtml += "<div class='description'>"+val.description+"</div>"
                      thtml += "<div class='price'>"+val.price+"</div>"
                      thtml += "</a></div>"
                    });
                break;
            }
            $('.ads-content').empty().html(thtml);

        });
        // event.preventDefault();              
    });

    $('#modalCreateAds').on('hidden.bs.modal', function () {
        var thtml = "";
        $.getJSON("menu.json",{}, function( data ){ 
        jQuery.each(data['default'], function(i, val) {
          thtml += "<div class='option-item col'>"
          thtml += "<div class='image'><img src=images/"+val.image+"></div>"
          thtml += "<div class='title'>"+val.title+"</div>"
          thtml += "<div class='description'>"+val.description+"</div>"
          thtml += "<div class='price'>"+val.price+"</div>"
          thtml += "</div>"
        });
        $('.ads-content').empty().html(thtml);

        });
        
    }) 

    //ONBOARDING

    var stepOnboard = 0;
    var allOnboard = [];
    var tot = $(".onboard").length;
    if( ($(".onboard").length) && (window.location.href.indexOf("dashboard") > -1) && (stepOnboard < tot)){
        var tempData = [];
        
        var step = 1;
        $(".onboard").each(function(){
            var thtml = "";
            if(step==1){
                $(".onboarding-overlay").show();
                $(this).addClass("nowactive")
            }
            switch($(this).data("onboard")){
                case "center" : thtml += "<div class='onboard-item onboarding-center'>";
                                break;
                case "top" : thtml += "<div class='onboard-item onboarding-top'>";
                                break;
                case "right" : thtml += "<div class='onboard-item onboarding-right'>";
                                break;
                case "left" : thtml += "<div class='onboard-item onboarding-left'>";
                                break;
                default : thtml += "<div class='onboard-item onboarding-bottom'>";
                                break;
            };
            thtml += "<div class='step-ctr'>step "+step+" of "+tot+"</div>";
            thtml += "<div class='title'>"+ $(this).data("title") +"</div>"; 
            thtml += "<div class='content'>"+ $(this).data("description") +"</div>"; 
            if(step==1){
                thtml += "<div class='cta-btn'><a class='next-btn'>Berikutnya</a></div>"; 
                thtml += "<a class='skip-btn'>Skip Tutorial</a>"; 
            }else{
                thtml += "<div class='cta-btn'><a class='prev-btn'>Kembali</a><a class='next-btn'>Berikutnya</a></div>"; 
                thtml += "<a class='skip-btn'>Skip Tutorial</a>"; 
            }
            $(this).append(thtml);    
            step += 1;

        });

        $(document).on('click','.skip-btn',function(e) {
            stepOnboard = tot;
            $(".onboard").removeClass('nowactive');
            $(".onboarding-overlay").fadeOut();
        });
        
        
        //IF BARU DAFTAR SHOW ONBOARD 
        $(document).on('click','.next-btn',function(e) {
            $(this).closest('.onboard').removeClass("nowactive");
            if(stepOnboard == 0){
                allOnboard = $(this).closest('.onboard').removeClass("nowactive").siblings().find('.onboard');
                $(allOnboard[stepOnboard]).addClass('nowactive');
                stepOnboard = 1;
                $('nav.dashnavbar').css('z-index', '2020');
            }else{
                stepOnboard += 1;
                $(allOnboard[stepOnboard-1]).addClass('nowactive');
                if(stepOnboard >= tot){
                    $(".onboarding-overlay").fadeOut();
                }

            }
            // $('html,body').animate({
            //     scrollTop: $(allOnboard[stepOnboard]).find(".onboard-item").offset().top - 80
            // }, 700);    
            $('li.nav-item.dropdown').removeClass("show");
            $('.dropdown-menu').removeClass("show");
            
        });

        $(document).on('click','.prev-btn',function(e) {
            // if(stepOnboard == tot){
            //     allOnboard = $(this).closest('.onboard').removeClass("nowactive").siblings().find('.onboard');
            // };
            // $('html,body').animate({
            //     scrollTop: $(allOnboard[stepOnboard]).find(".onboard-item").offset().top - 80
            // }, 700);    
            $(this).closest('.onboard').removeClass("nowactive");
            
            stepOnboard -= 1;
            if(stepOnboard < 1){
                stepOnboard = 1;                
            }
            $(allOnboard[stepOnboard-1]).addClass('nowactive');
            
            $('li.nav-item.dropdown').removeClass("show");
            $('.dropdown-menu').removeClass("show");


            // $(this).closest('.onboard').removeClass("nowactive");
            // if(stepOnboard >= 1){
            //     stepOnboard -= 1;
            // }else{
            //     $(".onboarding-overlay").fadeOut();
            // };
            // $(allOnboard[stepOnboard]).addClass('nowactive');
            
        });

    }

    //END OF ONBOARDING
    
    $('nav li.dropdown').hover(function() {
        if($(".onboarding-overlay").is(":hidden")){
            $(this).addClass('show');
            $(this).children('.dropdown-menu').addClass('show');                
        }
    }, function() {
        if($(".onboarding-overlay").is(":hidden")){
            $(this).removeClass('show');
            $(this).children('.dropdown-menu').removeClass('show');
        }
    });
    
    $("#createGroupContent").click(function(e){
        $("fieldset.fieldset-inside").slideToggle();
        e.preventDefault();
    });

    $("#createGroupProfile").click(function(e){
        $("fieldset.create-profile").slideToggle();
        e.preventDefault();
    });

    $(".cancel-btn").click(function(e){
        $(this).closest('.fieldset-inside').slideToggle();
        e.preventDefault();
    });

    $(".add-location-btn").click(function(e){
        $(".new-location").slideToggle();
        e.preventDefault();
    });

    $(".create-group").click(function(e){
        var temp = $(".initial-group").html();
        $(".group-wrapper").append('<fieldset class="fillform additional-form w-100">'+temp+'</fieldset>');
        $("fieldset .archive").hide();
        $(".initial-group").hide();
        $(".summary-group").show();
        e.preventDefault();
    });
    
    //MMS Template Image
    $("#upload-input").change(function(){
        // readURL(this);
        if (this.files && this.files[0]) {
        console.log("masuk");
            var reader = new FileReader();            
            reader.onload = function (e) {
                $('.upload-area').css('background-image','url('+ e.target.result +')');
                // $('.upload-area').empty();
            }            
            reader.readAsDataURL(this.files[0]);
        }

    });

    $(".select-option").click(function(e){
        //DUMMY PURPOSE menampilkan data
        if($(this).hasClass('select-content')){
            // var temp_content = '<div class="body"><div class="row"><div class="col-sm-10">Content Promo Beli 1 dapat 1</div><div class="item item-action col-sm-2"><a href="" title="Edit"><i class="far fa-edit"></i></a> <a href="" title="Delete"><i class="far fa-trash-alt"></i></a></div></div></div>';        
            
            var send = {showdata:'newcontent'};
            $.post( "../../../showdata.php", send)
            .done(function( data ) {
                $( ".archive.list-group" ).prepend( data ).css('margin-top','20px');   
                $( ".archive.list-group" ).fadeIn()
            });     
            $('fieldset.create-new').slideUp();
            e.preventDefault();

        }
        // $(this).parent().parent().siblings(".list-group").append(temp_content).fadeIn();
    });

    $("#morelocation").click(function(e){
        var newelem = '<div class="custom-select"><select class="form-control"><option>Pilih Location</option><option>Location 1</option><option>Location 2</option></select></div>';
        $(".morelocation").append(newelem);
    });

    $("#moreprofile").click(function(e){
        var newelem = '<div class="custom-select"><select class="form-control"><option>Pilih Profile</option><option>Profile 1</option><option>Profile 2</option></select></div>';
        $(".moreprofile").append(newelem);
    });

    $(".btn-addcity").click(function(e){
        var init = $(".initial-city").html();
        var str_city = '<div class="col-sm-4"><div class="custom-select"><select class="form-control" data-placeholder="Pilih Kota"><option></option><option>Jakarta</option></select></div></div>';
        var str_kecamatan = '<div class="col-sm-4"><div class="custom-select"><select class="form-control" data-placeholder="Pilih Kecamatan"><option></option><option>Ragunan</option></select></div></div>';
        var str_kelurahan = '<div class="col-sm-4"><div class="custom-select"><select class="form-control" data-placeholder="Pilih Kelurahan"><option></option><option>Warung Jati</option></select></div></div>';

        var thtml = "<div class='form-group row addcity-item'>"+str_city+str_kecamatan+str_kelurahan+"<div class='remove-city'><i class='fas fa-trash-alt'></i></div></div>";
        $('.city-container').append(thtml);
        var lastitem = $('.city-container .addcity-item').last();
        lastitem.find('select.form-control').select2({
            placeholder: 'Pilih',
            allowClear: true,
            minimumResultsForSearch: 5
        });

    });


    $(".give-voucher").click(function(){
        $(".voucher-wrapper").slideToggle();
    });

    $("#upload-voucher").change(function(){
        var file = $('#upload-voucher')[0].files[0];
        if (file){ $(".btn-upload-voucher").html(file.name) }
    });

    $(".browsing-behaviour").click(function(){
        $(".browsing-behave-wrapper").slideToggle();
    });

    $(".phonecall-behaviour").click(function(){
        $(".phonecall-behave-wrapper").slideToggle();
    });

    $(".nominal li > div").click(function(e){
        $(".nominal li > div").removeClass('selected');
        $(this).addClass('selected');
    });

    if($('.topup-steps').length){
        $('.topup-steps').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrow: false,
            infinite: false,
            adaptiveHeight: true,
            draggable: false
        });
    };

    $(".btn-topup-next").click(function(e){
        $('.topup-steps').slick('slickNext');
        e.preventDefault();
    });

    $(".btn-topup-prev").click(function(e){
        $('.topup-steps').slick('slickPrev');
        e.preventDefault();
    });

    $("#set-profile-img").change(function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();            
            reader.onload = function (e) {
                $('.profile-image').css('background-image','url('+ e.target.result +')');
            }            
            reader.readAsDataURL(this.files[0]);
        }

    });

    $(".document-picture input[type=file]").change(function(e){
        if (this.files && this.files[0]) {
            var reader = new FileReader();  
            var elem = $(this);
            console.log(elem);
            reader.onload = function (e) {
                
                elem.siblings('.document-image').css('background-image','url('+ e.target.result +')');
            }            
            reader.readAsDataURL(this.files[0]);
        }

    });

    $("select.select-group").change(function(e){
        var selected = $(this)[0].selectedIndex;
        if( (selected==1)||(selected==0)) {
            console.log($(this).closest('fieldset').find(".select-option"));
            $(this).closest('fieldset').find(".select-option").hide();
            // $("fieldset.create-new").find(".input-group-name").hide();
        }else{
            $(this).parent().parent().siblings().find(".select-option").show().css('display','block');
            $("fieldset.create-new").find(".input-group-name").show();
        }
        $(this).parent().parent().parent().siblings(".archive").empty().hide();
    });

    var isicontent;
    $(document).on('click','.item-action a[title=Edit]',function(e) {
        var cur = $(this);
        var temp = $(this).closest(".archive");
        isicontent = $(this).closest(".row").html();
        var send = {};
        if(temp.hasClass("archive-content")){
            send = {showdata:'editcontent'};
        }
        if(temp.hasClass("archive-profile")){
            send = {showdata:'editprofile'};
        }

        $.post( "../../../showdata.php", send)
        .done(function( data ) {
            var temp = cur.closest('.row');
            cur.closest('.body').html('<div class="editor">'+ data +'</div>').find('fieldset').slideToggle();
        });            
        e.preventDefault();
    });

    $(document).on('click','.btn-close-editor',function(e) {
        var temp = $(this).closest('.body').find(".input-group-name").attr('placeholder');

        // $(this).closest('.body').find('.editor').remove();
        $(this).closest('.body').find('fieldset').slideToggle('slow',function(){
            $(this).closest('.body').html('<div class="row">'+ isicontent +'</div>');
        })
        e.preventDefault();
    });

    $(".btn-add-batch").click(function(e){
        var init = $(".initial-batch").html();
        var thtml = "<div class='row batch-item'>"+init+"<div class='delete-row'><i class='far fa-trash-alt'></i></div></div>";
        $('.batch-container').append(thtml);
    });

    $(".btn-add-segment").click(function(e){
        var init = $(this).closest('fieldset').find(".initial-segment").html();
        var thtml = "<div class='row segment-item'>"+init+"<div class='delete-row d-flex align-items-center'><i class='far fa-trash-alt'></i></div></div>";
        $(this).closest('fieldset').find('.segment-container').append(thtml);
    });

    $(document).on('click','.batch-container .delete-row i',function(e) {
        $(this).closest(".row").fadeOut('fast',function(){
            $(this).remove();
        });
    });
    
    $(document).on('click','.segment-container .delete-row i',function(e) {
        $(this).closest(".row").fadeOut('fast',function(){
            $(this).remove();
        });
    });
    
    $('#modalTopup').on('hide.bs.modal', function (e) {    
        $('.topup-steps').slick('slickGoTo',0,true);
    });

    if($('.addreceiver-steps').length){
        $('.addreceiver-steps').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrow: false,
            infinite: false,
            adaptiveHeight: true,
            draggable: false
        });
    };

    $(".btn-addreceiver-next").click(function(e){
        var selected = $("input:radio[name='addreceiver']");
        var radioIndex = selected.index(selected.filter(':checked'));
        if(radioIndex==0){
            // console.log('masuk1');
            $('.addreceiver-steps').slick('slickNext');
        }else{
            // console.log('masuk2');
            window.location.href = 'http://localhost/myads';            
        }
        e.preventDefault();
    });

    $(".btn-topup-prev").click(function(e){
        $('.topup-steps').slick('slickPrev');
        e.preventDefault();
    });

    $('#modalGlobal').on('show.bs.modal', function (e) {    
        var source = $(e.relatedTarget).data('invoker');   
        $.getJSON("global-notification.json",{}, function( data ){ 
            $('#modalGlobal .modal-title').html(data[source][0].title);
            $('#modalGlobal .modal-body').html(data[source][0].content);
        });
    });

    $(".navbar-toggler").click(function(e){
        if(swidth<540){
            $(this).closest('.navbar').toggleClass('mobile');        
            $('nav.navbar').find('.img-logo').attr('src','images/myads-logo-color.png')
            e.preventDefault();
        }
    });    

    if($('.banner-slider').length){
        $('.banner-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrow: false,
            autoplay: true,
            pauseOnHover: false,
            dots: true
        });

        $('.banner-slider').on('afterChange', function(slick, currentSlide){
            console.log(currentSlide);
        });

    };

    if($('select.form-control').length){
        $('select.form-control').select2({
            placeholder: 'Pilih',
            allowClear: true,
            minimumResultsForSearch: 5
        });
    }

    $(".btn-msisdn").click(function(e){
        $("ul.msisdn-number").append("<li>"+$("input.msisdn-input").val()+" <i class='fas fa-times-circle'></i></li>");
        e.preventDefault();
    });        

    $(document).on('click','.msisdn-number > li > i',function(e) {
        $(this).parent().remove();
    });
    
    $(".change-template").click(function(e){
        $('#modalTemplate').modal('show');            
        e.preventDefault();
    });

    if($('.compare-slider').length){
        $('.compare-slider').slick({
            slidesToShow: 3,
            dots: false,
            arrows: true,
            infinite: false,
            slidesToScroll: 3          
        });
    }

    $(document).on('click','.remove-city',function(e) {
        $(this).parent().remove();
    });

    var maxHeight = -1;
    $('.slick-slide').each(function() {
      if ($(this).height() > maxHeight) {
        maxHeight = $(this).height();
      }
    });
    $('.slick-slide').each(function() {
      if ($(this).height() < maxHeight) {
        $(this).css('margin', Math.ceil((maxHeight-$(this).height())/2) + 'px 0');
      }
    });

    if($('#datetimepicker1').length){
        $('#datetimepicker1').datetimepicker({format: 'L'});
        $('#datetimepicker2').datetimepicker({format: 'LT'});
        $('#datetimepicker3').datetimepicker({format: 'L'});
        $('#datetimepicker4').datetimepicker({format: 'LT'});
    }

    $('#agree-terms').click(function(e){//CEK APAKAH FORM TERISI SEMUA
        if( this.checked ){
            var empty = $('.needvalidate').find("input").filter(function() {
                return this.value === "";
            });
            console.log(empty.length);
            if(empty.length == 1) {
                $('button').prop('disabled',false);
                $('button').addClass('btn-yellow');                
            }else{
                $('button').removeClass('btn-yellow');
                $('button').prop('disabled',true);                
            }
        }else{
                $('button').removeClass('btn-yellow');
                $('button').prop('disabled',true);
        }
    });

    $('.loginform input').keypress(function(e){ //CEK APAKAH FORM TERISI SEMUA
        var empty = $('.needvalidate').find("input").filter(function() {
            return this.value === "";
        });
        if(empty.length == 0) {
            $('button').prop('disabled',false);
            $('button').addClass('btn-yellow');                
        }else{
            $('button').removeClass('btn-yellow');
            $('button').prop('disabled',true);                
        }
    });

    var validEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    $(".fillform form [required='']").on('input', function(){
        console.log('sds');
        var len = $(this).val();
        if( $(this).attr('type')=='email' ){
            if (validEmail.test(this.value)){
                $(this).closest('.form-group').find('.icon-validate').removeClass("false fa-circle").addClass("true fa-check-circle");
                $(this).closest('.form-group').find('.warning-msg').empty();
            }else{
                $(this).closest('.form-group').find('.icon-validate').removeClass("true fa-check-circle").addClass("false fa-check-circle");
                $(this).closest('.form-group').find('.warning-msg').html('Email anda belum valid');
            }
        }else{
            if(len.length >= 1){
                $(this).closest('.form-group').find('.icon-validate').removeClass("false fa-circle").addClass("true fa-check-circle");
            }else{
                $(this).closest('.form-group').find('.icon-validate').removeClass("true fa-check-circle").addClass("false fa-check-circle");
            };

        }
    });

    var audience = 0;
    $('#btn-add-audience').click(function(e){
        if(audience <= 2 ){
        $('#add-audience').append("<fieldset class='selector-profile'>"+$('fieldset.selector-profile').html()+"</fieldset>" );
        $('#add-audience').each('select.form-control').select2();        
        audience += 1;
        e.preventDefault();
        }
    });

    $(".xml-input").on('input', function(){
        $(".btn-check-xml").removeClass('active');
    });

    $('#modalVast').on('show.bs.modal', function (e) {
      $('#show-xml .xml-view').hide();
      $('#show-xml .xml-extract').hide();
      $(".btn-check-xml").removeClass('active');
    })

    $(".btn-check-xml").on('click', function(e){
        $(this).addClass('active');
        id = e.target.id;
        if(id=='xml-view'){
            $.ajax({
            url: 'http://essmalang.ddns.net/nufolder/vast.xml', // name of file you want to parse
            dataType: "xml", 
            success: function(data){
                $('#show-xml textarea').val(new XMLSerializer().serializeToString(data));
                $('#show-xml .xml-extract').hide();
                $('#show-xml .xml-view').show();
            },
            error: function(){alert("Error: Something went wrong") }
            });            
        }else if(id=='xml-extract'){
            $('#show-xml .xml-view').hide();
            $('#show-xml .xml-extract').show();
        };
        $(this).siblings('button').removeClass('active');
    });

    $('input[name="selectvast"]').change(function() {
        $('.vast-selected').fadeOut();
    })

    $("#btn-take-content").on('click', function(e){
        $('.vast-selected').fadeIn();
    });

    $(".add-media").on('click', function(e){

        var val = $(this).closest('fieldset').find('select[name=placeholder-type]').val();
        if(val == 'rect'){
            $('#modalRectangle').modal({
                backdrop: 'static'
            });
        }else if(val == 'vast'){
            $('#modalVast').modal({
                backdrop: 'static'
            });
            
        }
    });

});

$( window ).scroll(function() {
    menuscrolled();
    $(".animated").each(function(){
        elem = $(this);
        if( ( elem.offset().top - $(window).scrollTop() ) <= $(window).height()-threshold ){
                elem.css('opacity',1);
                elem.addClass(elem.attr('anim'));
        }
    });
});


