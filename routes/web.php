<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/balance-top-up', 'BalanceTopUp@index');
Route::post('/balance-top-up', 'BalanceTopUp@store');
Route::get('/balance-top-up/req', 'BalanceTopUp@req')->name('balance-top-up.request');

Route::get('/balance-detail', 'BalanceDetail@index');
Route::get('/balance-detail/req', 'BalanceDetail@req');